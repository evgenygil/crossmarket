<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $user_id
 * @property int $platform_id
 * @property string $created_at
 * @property string $updated_at
 * @property User $user
 * @property Platform $platform
 */
class Credential extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['user_id', 'platform_id', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function platform()
    {
        return $this->belongsTo('App\Platform');
    }
}
