<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $feed_url
 * @property string $type
 * @property string $feed_status
 * @property string $created_at
 * @property string $updated_at
 * @property UsersFeedsPlatform[] $usersFeedsPlatforms
 */
class Feed extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['feed_url', 'type', 'feed_status', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function usersFeedsPlatforms()
    {
        return $this->hasMany('App\UsersFeedsPlatform');
    }
}
