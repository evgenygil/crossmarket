<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $ufp_id
 * @property string $status
 * @property string $report_status
 * @property string $created_at
 * @property string $updated_at
 * @property UsersFeedsPlatform $usersFeedsPlatform
 */
class Report extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['ufp_id', 'status', 'report_status', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function usersFeedsPlatform()
    {
        return $this->belongsTo('App\UsersFeedsPlatform', 'ufp_id');
    }
}
