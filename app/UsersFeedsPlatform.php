<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $platform_id
 * @property int $user_id
 * @property int $feed_id
 * @property string $ufp_status
 * @property string $created_at
 * @property string $updated_at
 * @property User $user
 * @property Platform $platform
 * @property Feed $feed
 * @property Report[] $reports
 */
class UsersFeedsPlatform extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['platform_id', 'user_id', 'feed_id', 'ufp_status', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function platform()
    {
        return $this->belongsTo('App\Platform');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function feed()
    {
        return $this->belongsTo('App\Feed');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function reports()
    {
        return $this->hasMany('App\Report', 'ufp_id');
    }
}
