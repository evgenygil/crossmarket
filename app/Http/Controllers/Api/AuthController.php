<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\User;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    /**
     * The request instance.
     *
     * @var \Illuminate\Http\Request
     */
    private $request;
    private $salt;
    /**
     * Create a new controller instance.
     *
     * @param Request $request
     */

    public function __construct(Request $request) {
        $this->request = $request;
        $this->salt = '70fd96e5e98c07730aa5e8a66bcf8273';
    }

    protected function jwt(User $user) {
        $payload = [
            'iss' => "lumen-jwt", // Issuer of the token
            'sub' => $user->id, // Subject of the token
            'iat' => time(), // Time when JWT was issued.
            'exp' => time() + 60*60 // Expiration time
        ];

        // As you can see we are passing `JWT_SECRET` as the second parameter that will
        // be used to decode the token in the future.
        return JWT::encode($payload, env('JWT_SECRET'));
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login() {
        $this->validate($this->request, [
            'email'     => 'required|email',
            'password'  => 'required'
        ]);


        // Find the user by email
        $user = User::where('email', $this->request->input('email'))->first();

        if (!$user) {

            // You wil probably have some sort of helpers or whatever
            // to make sure that you have the same response format for
            // differents kind of responses. But let's return the
            // below respose for now.

            return response()->json([
                'error' => 'Email does not exist.'
            ], 400);
        }
        // Verify the password and generate the token
        if (Hash::check($this->request->input('password'), $user->password)) {
            return response()->json([
                'token' => $this->jwt($user)
            ], 200);
        }
        // Bad Request response
        return response()->json([
            'error' => 'Email or password is wrong.',
            'pass_comparison' => Hash::check($this->request->input('password'), $user->password),
            'pass' => $this->request->input('password'),
            'hash' => Hash::make($user->password),
            'password' => $user->password,
        ], 400);

    }

    /**
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function signup() {
        $this->validate($this->request, [
            'email'     => 'required|email',
            'password'  => 'required'
        ]);

        if ($this->request->has('email') && $this->request->has('password')) {

            $user = new User();
            $user->email = $this->request->input('email');
            $user->name = $this->request->input('name');
            $user->last_login = date('Y-m-d G:i:s');
            $user->active = true;
            $user->status = 'new';
            $user->password = Hash::make($this->request->input('password'));

            if ($user->save()) {
                return response()->json(['method' => 'signup', 'status' => 'successfully']);
            } else {
                return response()->json(['method' => 'signup', 'status' => 'failed']);
            }
        } else {
            return response()->json(['method' => 'signup', 'status' => 'failed']);
        }

//        handling stuff
       return response()->json(['method' => 'signup', 'payload' => $this->request->all()]);
    }

    public function logout() {
        return response()->json(['method' => 'logout']);
    }

}
