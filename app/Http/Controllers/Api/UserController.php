<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => 'index']);
    }

    public function version() {
        return response()->json(['v' => '1']);
    }

    public function profile(Request $request) {

        return response()->json(['user' => $request->auth->id, 'token' => 'verified']);
    }

    public function index() {
        return response()->json(['user' => 'all']);
    }

    public function view($id) {
        return response()->json(['user' => 'user # ' . $id]);
    }

}
