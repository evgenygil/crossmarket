<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 * @property string $password
 * @property string $salt
 * @property string $email
 * @property string $last_login
 * @property boolean $active
 * @property string $status
 * @property string $created_at
 * @property string $updated_at
 * @property Credential[] $credentials
 * @property UsersFeedsPlatform[] $usersFeedsPlatforms
 */
class User extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['name', 'password', 'email', 'last_login', 'active', 'status', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function credentials()
    {
        return $this->hasMany('App\Credential');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function usersFeedsPlatforms()
    {
        return $this->hasMany('App\UsersFeedsPlatform');
    }
}
