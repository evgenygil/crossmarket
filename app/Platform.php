<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 * @property mixed $options
 * @property string $created_at
 * @property string $updated_at
 * @property Credential[] $credentials
 * @property UsersFeedsPlatform[] $usersFeedsPlatforms
 */
class Platform extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['name', 'options', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function credentials()
    {
        return $this->hasMany('App\Credential');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function usersFeedsPlatforms()
    {
        return $this->hasMany('App\UsersFeedsPlatform');
    }
}
