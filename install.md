Preparing work environment
```
sudo apt-get install php7.2-pgsql
```

Nginx config
```
server {
    listen 80 default_server;
    listen [::]:80 default_server ipv6only=on;
    
    root /home/gil/Projects/crossmarket/public;
    index index.php index.html index.htm;

    server_name crossmarket.local;

    location / {
        try_files $uri $uri/ /index.html;
    }

    location /api {
        try_files $uri $uri/ /index.php?$query_string;
    }

    location ~ \.php$ {
        try_files $uri /index.html =404;
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
	fastcgi_pass unix:/run/php/php7.1-fpm.sock;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        include fastcgi_params;
    }
}
```