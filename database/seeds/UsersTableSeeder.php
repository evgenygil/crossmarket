<?php
/**
 * Created by PhpStorm.
 * User: gil
 * Date: 13.03.19
 * Time: 8:54
 */

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
             'name' => 'Admin',
             'email' => 'admin@mail.ru',
             'password' => \Illuminate\Support\Facades\Hash::make('qwerty123'),
             'last_login' => date('Y-m-d H:i:s'),
             'active' => true,
             'status' => 'admin',
             'salt' => 10
         ]);
    }
}