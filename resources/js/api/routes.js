/* eslint-disable */
import axios from "axios";
import config from "../config";

const api = axios.create({ baseURL: config.api });

api.interceptors.request.use(
    (config) => {
        let token = localStorage.getItem('token');

        if (token) {
            config.headers['token'] = token;
        }

        return config;
    },

    (error) => {
        return Promise.reject(error);
    }
);

export default {

    getProfile: async function () {
        try {
            let res = await api.get('user/profile');
            return res.data;
        } catch (e) {
            return e
        }
    },

    login: ({commit}, user) => {
        return new Promise((resolve, reject) => {
            commit('auth_request');
            axios({url: config.api + 'auth/login', data: user, method: 'POST'})
                .then(resp => {
                    const token = resp.data.token;
                    const user = resp.data.user;
                    localStorage.setItem('token', token);
                    axios.defaults.headers.common['Authorization'] = token;
                    commit('auth_success', token, user);
                    resolve(resp)
                })
                .catch(err => {
                    commit('auth_error');
                    localStorage.removeItem('token');
                    reject(err)
                })
        })
    },

    signup: ({commit}, user) => {
        return new Promise((resolve, reject) => {
            commit('auth_request');
            axios({url: config.api + 'auth/signup', data: user, method: 'POST'})
                .then(resp => {
                    const token = resp.data.token;
                    const user = resp.data.user;
                    localStorage.setItem('token', token);
                    localStorage.setItem('token', token);
                    axios.defaults.headers.common['Authorization'] = token;
                    resolve(resp)
                })
                .catch(err => {
                    commit('auth_error', err);
                    localStorage.removeItem('token');
                    reject(err)
                })
        })
    }
}